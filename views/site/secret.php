<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Секретная страница';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <iframe width="560" height="315" src="https://www.youtube.com/embed/npvNPORFXpc" frameborder="0" allowfullscreen></iframe>
</div>

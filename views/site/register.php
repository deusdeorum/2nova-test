<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

$this->title = 'Регистрация нового пользователя';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-register']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <?= $form->field($model, 'firstName') ?>
                <?= $form->field($model, 'lastName') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'birthDate')->widget(DatePicker::classname(), ['language' => 'ru', 'dateFormat' => 'dd.MM.yyyy', 'options' => ['class' => 'form-control'],
                'clientOptions'=> ['showAnim' => 'slideDown', 'changeYear' => 'true', 'changeMonth' => 'true', 'yearRange' => '1900:']]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * RegisterForm is the model behind the registration form.
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;
    public $email;
    public $firstName;
    public $lastName;
    public $birthDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required', 'message' => 'Имя пользователя — обязательное поле.'],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'К сожалению, имя уже занято.'],
            ['username', 'string', 'min' => 2, 'max' => 32, 'tooShort' => 'Имя пользователя не может быть менее 2 символов.',
            'tooLong' => 'Максимальная длина имени пользователя — 32 символа.'],
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Имя пользователя не должно начинаться '.
                                                        'с цифры, и должно состоять только из цифр и латинских букв.'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Электронная почта — обязательное поле.'],
            ['email', 'email', 'message' => 'Введите корректный e-mail адрес.'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'Пользователь с такой электронной почтой уже существует.'],



            ['password', 'required', 'message' => 'Пароль — обязательное поле.'],
            ['password', 'string', 'min' => 6, 'max' => 32, 'tooShort' => 'Длина пароля не может быть менее 6 символов.',
                'tooLong' => 'Максимальная длина пароля — 32 символа.'],

            ['password_repeat', 'required', 'message' => 'Повторите пароль.'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message' => 'Введенные пароли не совпадают.'],

            ['firstName', 'required', 'message' => 'Имя — обязательное поле.'],
            ['lastName', 'string', 'max' => 64, 'tooLong' => 'У вас слишком длинная фамилия.'],

            ['birthDate', 'default', 'value' => null],
            ['birthDate', 'date', 'format' => 'dd.MM.yyyy', 'message' => 'Дата указана в неправильном формате.'],


        ];
    }

    /**
     * @return array the attribute labels.
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя (логин)',
            'password' => 'Пароль',
            'password_repeat' => 'Пароль еще раз',
            'email' => 'Электронная почта',
            'firstName' => 'Имя',
            'lastName' => 'Фамилия',
            'birthDate' => 'Дата рождения',
        ];
    }

    /**
     * Registers user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signUp()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->email = $this->email;
            $user->firstName = $this->firstName;
            $user->lastName = $this->lastName;
            $user->birthDate = $this->birthDate;
            if ($user->save()) {
                return $user;
            }
        }
        return null;
    }







}
